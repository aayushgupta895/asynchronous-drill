/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/
const fs = require("fs");


function writingToFile(filename, data, callback) {
  fs.writeFile(filename, data, (err) => {
    if (err) {
      console.log(`error occurred while writing uppercase data `, err);
    } else {
      callback(null);
    }
  });
}

function readingToFile(filePath, callback) {
  fs.readFile(filePath, "utf8", (err, data) => {
    if (err) {
      console.log(`error occurred while writing uppercase data `, err);
    } else {
      callback(data);
    }
  });
}

function appendingToFile(destination, data, callback) {
  fs.appendFile(destination, `${data}\n`, (err) => {
    if (err) {
      console.log(
        `error occurred while appending the name of the file to filenames file`
      );
    } else {
      callback();
    }
  });
}

function fsProblem2(path) {
  readingToFile(path, (data) => {
    const newData = data.toUpperCase();
    creatingUppercaseFile(newData);
  });
}

function creatingUppercaseFile(data) {
  writingToFile("upperCaseFile.txt", data, () => {
    console.log(`uppercase file has been created`);
    appendingToFile("filenames.txt", "upperCaseFile.txt", () => {
      console.log(`uppercasefile.txt stored in filenames.txt`);
      creatingLowerCaseFile();
    });
  });
}

function creatingLowerCaseFile() {
  readingToFile("upperCaseFile.txt", (data) => {
    data = data.toLowerCase();
    writingToFile("lowerCaseFile.txt", data, () => {
      console.log(`lowercase file has been created`);
      appendingToFile("filenames.txt", "lowerCaseFile.txt", () => {
        console.log(`lowercase stored in filenames.txt`);
        createSentence(data);
      });
    });
  });
}

function createSentence(data) {
  let writingCompletion = 0;
  const array = data.split(". ");
  array.forEach((sentence) => {
    appendingToFile("sentenceFile.txt", sentence, () => {
      writingCompletion++;
      console.log(`writing sentences`);
      if (writingCompletion == array.length) {
        appendingToFile("filenames.txt", "sentenceFile.txt", () => {
          console.log(`writing sentences complete`);
          sortAndAppend();
        });
      }
    });
  });
}

function sortAndAppend() {
  readingToFile("lipsum.txt", (data) => {
    let sortedArray = data.split(" ");
    sortedArray = sortedArray.sort();
    let arrLen = 0;
    sortedArray.forEach((arr) => {
      appendingToFile("sortedContent.txt", arr, () => {
      console.log(`writing sorted words`);
        arrLen++;
        if (arrLen == sortedArray.length) {
          appendingToFile("filenames.txt", "sortedContent.txt", () => {
            console.log(
              `sortedContent.txt filename has been stored in filenames.txt`
            );
            deleteAll();
          });
        }
      });
    });
  });
}


function deleteAll(){
  readingToFile('filenames.txt', (data)=>{
    data = data.split('\n');
    data.pop();
    let deleteFile = 0;
    data.forEach((filename)=>{
      fs.unlink(filename, (err)=>{
        if(err){
          console.log(`error while deleting the files`, err);
        }else{
          deleteFile++;
          if(deleteFile == data.length){
            console.log(`congrats all the files has been deleted`);
            fs.unlink('filenames.txt', (err)=>{
              if(err){
                console.log(`error while deleting filenames.txt`, err);
              }else{
                console.log(`filnames.txt deleted successfully`);
              }
            })
          }
        }
      })
    })
  })
}

module.exports = fsProblem2;