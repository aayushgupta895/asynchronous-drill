const {fsProblem1, writingFiles} = require('../fs-problem1.cjs');
(() => {
  const randomNumberOfFiles = [
    "index1.js",
    "index2.js",
    "index3.js",
    "index4.js",
    "index5.js",
  ];
  const absolutePathOfRandomDirectory = "dir";
  fsProblem1(absolutePathOfRandomDirectory, randomNumberOfFiles, writingFiles);
})();
