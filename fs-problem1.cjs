/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

const fs = require('fs');

function fsProblem1(dir, files, writingFiles){
    fs.mkdir(dir, {recursive : true}, (err)=>{
        if(err){
            console.log(`error occurred while creating a directory`);
        }else{
            writingFiles(dir, files, deletionCallback);
        }
    })
}

function writingFiles(dir, files, deletionCallback) {
  let completedWriting = 0;
  files.forEach((file) => {
    let timer = Math.floor(Math.random() * 5);
    setTimeout(() => {
      fs.writeFile(`${dir}/${file}`, "random data", (err) => {
        if (err) {
          console.log(`some error has occurred while writing file :`, file, err);
        } else {
          console.log(`finished writing the file `, file);
          completedWriting += 1;
          if (completedWriting == files.length) {
            console.log(`writing and creation of all the files is done`);
            deletionCallback(null, dir, files);
          }
        }
      });
    }, timer * 1000);
  });
}

function deletionCallback(err, dir, files) {
  if (err) {
    console.log(
      `an error has occurred inside the callback of wrting the files`
    );
  } else {
    let deletedFiles = 0;
    files.forEach((file)=>{
        fs.unlink(`${dir}/${file}`, (err)=>{
            deletedFiles++;
            if(err){
                console.log(`some error while deleting the file : `, err);
            }else{
                if(deletedFiles == files.length){
                    console.log(`all the files deleted successfully`);
                    fs.rmdir(dir, (err)=>{
                        if(err){
                            console.log(`error while deleting directory`);
                        }else{
                            console.log(`directory deleted successfully`);
                        }
                    })
                }
            }
        })
    })
  }
}


module.exports = {fsProblem1, writingFiles};
